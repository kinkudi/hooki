import webapp2
from wordgravity import jmain
import os


class Hooki(webapp2.RequestHandler):
	def get(self):
		path = './training'
		listing = os.listdir(path)
		summary = []
		self.response.out.write('<html><head><title>Hooki Results</title></head><body>') 
		for ifile in listing:
			ifilepath = '/'.join([path,ifile])
			data = open(ifilepath,'r').read()
			summary.append(''.join(['<h1>',ifilepath,'</h1>',jmain(data)]))
		self.response.out.write(''.join(['<br>'.join(summary),'</body></html>']))


app = webapp2.WSGIApplication([
	(r'/.*',Hooki)
	])

def main():
	app.run()

if __name__ == "__main__":
	main()




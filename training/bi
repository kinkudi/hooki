Proc. Nati. Acad. Sci. USA
Vol. 83, pp. 8333-8337, November 1986
Medical Sciences
Phosphorylation of 3'-azido-3'-deoxythymidine and selective
interaction of the 5'-triphosphate with human immunodeficiency
virus* reverse transcriptase
(antiviral chemotherapy/acquired immunodeficiency syndrome/thymidine kinase/thymidylate kinase)
PHILLIP A. FURMANt, JAMES A. FYFEf, MARTY H. ST. CLAIRt, KENT WEINHOLD��, JANET L. RIDEOUT?,
GEORGE A. FREEMAN?, SANDRA NUSINOFF LEHRMANt, DANI P. BOLOGNESI��, SAMUEL BRODER11,
HIROAKI MITSUYA 1, AND DAVID W. BARRYt
tDepartments of Virology, tExperimental Therapy, and TOrganic Chemistry, The Wellcome Research Laboratories, 3030 Cornwallis Road, Research Triangle
Park, NC 27709; ��Department of Surgery, Duke University Medical Center, Durham, NC 27710; and IIClinical Oncology Program, National Cancer Institute,
Bethesda, MD 20205
Communicated by George H. Hitchings, June 30, 1986
ABSTRACT The thymidine analog 3'-azido-3'-deoxythymidine (BW A509U, azidothymidine) can inhibit human immunodeficiency virus (HIV) replication effectively in the
50-500 nM range [Mitsuya, H., Weinhold, K. J., Furman,
P. A., St. Clair, M. H., Nusinoff-Lehrman, S., Gallo, R. C.,
Bolognesi, D., Barry, D. W. & Broder, S. (1985) Proc. Naul.
Acad. Sci. USA 82, 7096-7100]. In contrast, inhibition of the
growth of uninfected human fibroblasts and lymphocytes has
been observed only at concentrations above 1 mM. The nature
of this selectivity was investigated. Azidothymidine anabolism
to the 5'-mono-, -di-, and -triphosphate derivatives was similar
in uninfected and HIV-infected cells. The level of azidothymidine monophosphate was high, whereas the levels of the di- and
triphosphate were low (c5 MuM and <2 MuM, respectively).
Cytosolic thymidine kinase (EC 2.7.1.21) was responsible for
phosphorylation of azidothymidine to its monophosphate.
Purified thymidine kinase catalyzed the phosphorylations of
thymidine and azidothymidine with apparent K. values of 2.9
MuM and 3.0 ,uM. The maximal rate of phosphorylation with
azidothymidine was equal to 60% of the rate with thymidine.
Phosphorylation of azidothymidine monophosphate to the
diphosphate also appeared to be catalyzed by a host-cell
enzyme, thymidylate kinase (EC 2.7.4.9). The apparent Km
value for azidothymidine monophosphate was 2-fold greater
than the value for dTMP (8.6 ,uM vs. 4.1 MM), but the maximal
phosphorylation rate was only 0.3% of the dTMP rate. These
kinetic constants were consistent with the anabolism results and
indicated that azidothymidine monophosphate is an alternative-substrate inhibitor of thymidylate kinase. This conclusion
was reflected in the observation that cells incubated with
azidothymidine had reduced intracellular levels of dTTP. ICso
(concentration of inhibitor that inhibits enzyme activity 50%)
values were determined for azidothymidine triphosphate with
HYIV reverse transcriptase and with immortalized human
lymphocyte (H9 cell) DNA polymerase a. Azidothymidine
triphosphate competed about 100-fold better for the HIV
reverse transcriptase than for the cellular DNA polymerase a.
The results reported here suggest that azidothymidine is
nonselectively phosphorylated but that the triphosphate derivative efficiently and selectively binds to the HIV reverse
transcriptase. Incorporation of azidothymidylate into a growing DNA strand should terminate DNA elongation and thus
inhibit DNA synthesis.
Recently, Mitsuya et al. (1) reported that 3'-azido-3'-deoxythymidine (azidothymidine) was a selective and potent inhibitor of human T-cell lymphotropic virus type III (HTLVIII, now called human immunodeficiency virus, HIV) replication in several different lymphocyte cultures. Many other
thymidine analogs are known to inhibit other viruses in cell
culture with varying degrees of selectivity (2, 3). Selectivity
of activation can result from increased levels of cytosolic or
virus-encoded thymidine kinase in the infected cells. Although the monophosphate of some analogs can inhibit
thymidylate synthase, a more common target for interaction
is the virus-encoded DNA polymerase. Selectivity at this
level results from a higher affinity of the analog triphosphate
for the viral polymerase than for cellular polymerase(s).
Incorporation of the analog monophosphate into viral DNA
may be a critical event for some analogs (2).
Whether any of these modes of selectivity or targets of
inhibition pertain to the selective inhibition of HIV by
azidothymidine was not known. The retroviruses are not
known to code for a thymidine kinase, and the viral polymerase for which they code (RNA-directed DNA polymerase, reverse transcriptase, EC 2.7.7.49) is catalytically quite
distinct from cellular DNA polymerases (4, 5). An additional
aspect to the mode of action of azidothymidine is that the
3'-azido group should cause termination of DNA elongation
if the nucleoside moiety were incorporated into DNA.
Here we show that azidothymidine is phosphorylated to its
5'-triphosphate derivative by cellular enzymes of the thymidine-phosphorylation pathway [thymidine kinase (EC
2.7.1.21) and thymidylate kinase (EC 2.7.4.9)]. Further, we
show that azidothymidine 5'-triphosphate selectively competes for the viral reverse transcriptase, suggesting that the
reverse transcriptase of HIV can be a specific target for
antiviral chemotherapy.
MATERIALS AND METHODS
Cells and Viruses. Human foreskin fibroblasts (MRHF) and
human diploid embryonic lung fibroblasts (MRC5) were
obtained from Whittaker M.A. Bioproducts (Walkersville,
MD) and the American Type Culture Collection, respectively. Both cell cultures were grown in Eagle's minimal essential
medium (MEM) supplemented with 10% fetal bovine serum
Abbreviations: HIV, human immunodeficiency virus; PBL, peripheral blood lymphocyte.
*The Executive Committee of the International Committee on
Taxonomy of Viruses (ICTV) has endorsed the name human
immunodeficiency virus (to be abbreviated HIV) recently proposed
by a majority of the members of a study group of ICTV as
appropriate for the retrovirus isolates implicated as the causative
agents of acquired immunodeficiency syndrome [(1986) Science
232, 1486, and Nature (London) 321, 644].
8333
The publication costs of this article were defrayed in part by page charge
payment. This article must therefore be hereby marked "advertisement"
in accordance with 18 U.S.C. ��1734 solely to indicate this fact.8334 Medical Sciences: Furman et al.
(Sterile Systems, Logan, UT) and 2 mM L-glutamine. Vero
cells were grown in Eagle's MEM supplemented with 5%
newborn calf serum (Whittaker M.A. Bioproducts) and 5%
fetal bovine serum. Clone H9, an OKT4' cell line permissive
for HIV (6), was obtained from B. Hampar (National Cancer
Institute, Frederick Cancer Center) and grown in RPMI 1640
medium containing 20% fetal bovine serum. FG-10 cells (7)
were maintained in McCoy's 5-A medium supplemented with
10% fetal bovine serum. Peripheral blood lymphocytes
(PBLs), first stimulated for 48 hr with phytohemagglutinin
(PHA; Difco), were cultured in RPMI 1640 containing 10%6
delectinated interleukin 2 (Cellular Products, Buffalo, NY)
and supplemented with 10% fetal bovine serum. HIV was
obtained from the culture fluid of HIV-producing H9 cells (6)
kindly provided by R. Gallo (National Cancer Institute). The
number of viable cells was determined by dye exclusion with
erythrosin B.
Chemicals. Azido[5'-3H]thymidine was synthesized in
these laboratories by J. Hill and G. A. Freeman by unpublished procedures. Final purification (>99.9%o homogeneous)
was achieved by chromatography through a C18 reversedphase column with a 25-50% methanol gradient. The mono-,
di-, and triphosphates of azidothymidine were prepared from
azidothymidine by published methods (8-10). The phosphorylated derivatives of azidothymidine were isolated in these
laboratories by W. Miller (11). The monophosphate of
[3H]azidothymidine was synthesized with H9 cellular thymidine kinase (12) and was purified with a Mono Q column
(Pharmacia) at pH 2.8. Other chemicals were as used previously (12).
Cell Growth and Cytotoxicity Assays. The effect of
azidothymidine on cell growth was measured in a 72-hr
growth-inhibition assay described previously (13). Cell
cytotoxicity using clone H9 cells and phytohemagglutininstimulated PBLs was determined by erythrosin B exclusion
after incubation with various concentrations of azidothymidine for 72 hr at 37�XC. H9 cells and PBLs were cultured in
medium containing various concentrations of drug at 2 x 105
and 5 x 10- cells per ml, respectively.
Enzyme Preparations. Cell extracts were 10,000 x g supernatants of frozen-thawed (three cycles) H9 cell pellets in
four volumes of extraction buffer (10 mM Tris-HCl, pH
7.5/10 mM KCl/1 mM MgCl2/1 mM dithiothreitol). Cytosolic thymidine kinase and thymidylate kinase from H9 cells
were purified by affinity chromatography as described (12).
Enzyme was stored in elution buffer and desalted by sizeexclusion chromatography just before use. Less than 1%
dTMP- or dTDP-phosphohydrolyzing activity was detected
with either enzyme.
HIV reverse transcriptase was purified by a modification of
the procedure of Abrell and Gallo (14). In brief, 750 ml of
culture fluid harvested from HIV-infected H9 cells was
centrifuged at 18,000 rpm for 90 min in an R19 rotor
(Beckman) to pellet virus. Enzyme was extracted by incubating the virus pellet in buffer A [50 mM Tris-HCl, pH
7.9/0.25% Nonidet P-40/20 mM dithiothreitol/5% (vol/vol)
glycerol] containing 1 mM EDTA, 500 mM KCl, and 0.5%
deoxycholate. The enzyme was partially purified by passing
the extract through a DEAE-cellulose column (3 x 10 cm)
previously equilibrated with buffer A. Fractions containing
enzyme activity were dialyzed against buffer B (50 mM
Tris HCl, pH 7.9/50 mM NaCl/1 mM EDTA/1 mM
dithiothreitol/20% glycerol) and were further purified by
phosphocellulose chromatography. The peak fractions were
pooled and dialyzed against buffer B containing 50% glycerol.
To the dialyzed enzyme, bovine serum albumin was added to
give a final concentration of 1 mg/ml. The enzyme was
characterized as HIV reverse transcriptase based on its
cation, salt, pH, and template requirements (5). Cellular
DNA polymerase a was purified as described (15).
Enzyme Assays. Phosphorylation of thymidine, azidothymidine, or their monophosphates was measured at 370C by a
DEAE-paper disk method (16) or by measuring the conversion of the substrate to product by excision and scintillation
counting of the appropriate portions of polyethylenimine
thin-layer plates (12, 16) after development with solvent I
(50% methanol) or solvent 11 (0.3 M sodium formate, pH
3.4/50% methanol). Standard reaction mixtures for nucleoside phosphorylation contained 50 mM Tris HCl (pH 7.5), 5
mM ATP*Mg, 0.1 mM [14C]thymidine or [3H]azidothymidine
(90 and 700 cpm/pmol), and enzyme. For measurements of
nucleoside monophosphate phosphorylations, [14C]dTMP
(90 cpm/pmol) or [3H]azidothymidine monophosphate (700
cpm/pmol) was substituted for the radiolabeled nucleosides.
Enzyme activity was proportional to enzyme concentration
and time of reaction. The apparent Km (Ko) or Kis values were
determined as described (12). One unit of enzyme activity
was defined as the amount of enzyme that would convert 1
pmol of thymidine or dTMP to its phosphorylated product per
minute under the above standard conditions.
The purified HIV reverse transcriptase was assayed using
reaction conditions similar to those of Abrell and Gallo (14)
and adapted to the DEAE-paper disk assay (15). Reaction
mixtures (150 ,ul) contained 50 mM Tris HCl (pH 7.3); 100
mM KCl; 5 mM MgCl2; 1 mM dithiothreitol; 80 ,M each
dATP, dCTP, and dGTP; and 5.6 ,M [3H]dTTP (4800
cpm/pmol), unless otherwise stated. The template concentration was 20 ,ug/ml for poly(rA)-oligo(dT)j2_j8 (Pharmacia
P-L Biochemicals) and for activated calf thymus DNA.
Cellular DNA polymerase a was assayed as described (15).
ICs0 values (concentration of inhibitor that inhibits enzyme
activity by 50%) for HIV reverse transcriptase and H9 DNA
polymerase a were calculated using the Probit computer
program (17).
High-Performance Liquid Chromatography Analysis. Neutralized perchloric acid extracts of cells treated with 50 ,M
[3H]azidothymidine were analyzed by HPLC as described
(18). Azidothymidine nucleotides were separated by elution
with a linear gradient of 0.015-1 M KH2PO4 (pH 3.5),
developed over 110 min at a flow rate of 0.5 ml/min.
Intracellular levels of deoxyribonucleoside triphosphates
were quantitated as described (19).
Identification of Phosphorylated Products. Phosphorylated
products obtained from cells or from phosphorylation of
azidothymidine catalyzed by cell extracts were identified as
5'-phosphates of azidothymidine by treatment of the products with Crotalus atrox phosphodiesterase I (EC 3.1.4.1),
which hydrolyzed the di- and triphosphates completely to the
monophosphate, and then with C. atrox 5'-nucleotidase (EC
3.1.3.5) to produce the nucleoside analog. Subsequent cochromatography with azidothymidine through a C18 reversed-phase column (retention volume 5.8 ml in 30% methanol) confirmed that the analog was, in fact, azidothymidine.
RESULTS
Effects of Azidothymidine on Cell Growth and Viability. The
effect of azidothymidine on cell growth varied with cell type
(Table 1). Human fibroblasts and lymphocytes showed little
inhibition of growth except at very high concentrations (ID50
- 1000 ,M), whereas epithelial cells of murine and primate
origin were somewhat more sensitive (ID50 = 30 and 170 ,uM,
respectively). This variation in sensitivity to azidothymidine
appeared to correlate with levels of azidothymidine 5'-
triphosphate in these cells (unpublished observations).
Cytotoxicity assays with H9 cells and PBLs also required
high concentrations of azidothymidine (-500 ,M) to achieve
the 50% end point (Table 1).
Anabolism of Azidothymidine. The nucleotide profile revealed the presence of radioactivity in the mono-, di-, and
Proc. Natl. Acad. Sci. USA 83 (1986)Proc. Natl. Acad. Sci. USA 83 (1986) 8335
Table 1. Effect of azidothymidine on rate of cell growth and cell
viability
IDo*, ILM
Cells Derivation Growth Viability
MRHF Human foreskin 3500 ND
MRC5 Human embryonic lung 1700 ND
H9 Immortalized human T cells 1000 >1000t
PBLs Human peripheral blood ND 500
Vero African green monkey kidney 170 ND
FG-10 Mouse 3T3 subline 30 ND
ND, not determined.
*Concentration of inhibitor that reduces cell growth or cell viability
by 50%o following exposure to the inhibitor for 72 hr.
tAzidothymidine at 1000 AsM reduced H9 cell viability 30%o.
triphosphate regions of the chromatogram (Fig. 1). Occasionally, minor peaks at about 19 min (<1% of total) and 47 min
(<0.3% of total) were observed. The material at about 17 min
was azidothymidine and was calculated to be less than or
equal to the concentration of azidothymidine in the medium.
The major derivatives were identified as 5'-phosphates of
azidothymidine (see Materials and Methods). High concentrations of azidothymidine monophosphate were detected in
the uninfected and the HIV-infected H9 cells (Fig. 1 and
Table 2), whereas the levels of the diphosphate and triphosphate were low. By 24 hr these phosphorylated derivatives
had accumulated maximally. Even though the level of
azidothymidine monophosphate that accumulated during a
24-hr exposure to azidothymidine decreased somewhat
throughout the replication cycle of the virus, no significant
differences were noted in the levels of the di- and triphosphate derivatives. Increasing the time that the cells were
exposed to the drug did not result in higher levels of
phosphorylated derivatives.
The decrease in levels of the phosphorylated derivatives of
azidothymidine, after removal of the drug from the incubation medium, was measured during peak virus replication
(days 5-6). HIV-infected cells were incubated for 24 hr in
medium containing 50 ,uM [3H]azidothymidine, after which
the cells were washed and the incubation was continued in
drug-free medium. Perchloric acid extracts for HPLC analysis were then prepared at various times during the incubation. Under these conditions, the levels of the phosphorylated derivatives of azidothymidine declined rapidly with
time (Table 3). However, 4 hr after removal of azidothymidine, the intracellular concentration of the triphosphate was
still 1 ,uM.
azdTMP
300
1001
ATP6
|0 ] L X 4~~~~~~~GT 2 4
E
0 20 40 60 80 100
Retention time, min
FIG. 1. HPLC profile of an extract of HIV-infected H9 cells
incubated with 50 ,uM [3H]azidothymidine for 24 hr at day 3 of
infection. azdT, azidothymidine; azdTMP, azidothymidine 5'-monophosphate; azdTDP, azidothymidine 5'-diphosphate; azdTTP, azidothymidine S'-triphosphate.
Table 2. Anabolism of azidothymidine in uninfected and
HIV-infected H9 cells
Concentration, pmol per 106 cells (,AM)
Cells Monophosphate Diphosphate Triphosphate
Uninfected 660 (790) 3.5 (4.2) 1.5 (1.8)
Infected
24 hr 460 (560) 2.6 (3.1) 0.9 (1.1)
Days 3-4 340 (410) 2.0 (2.4) 1.0 (1.3)
Days 6-7 170 (200) 4.4 (5.3) 1.7 (2.0)
Days 9-10 170 (210) 4.7 (5.6) 0.9 (1.1)
H9 cells were infected with HTLV-IIIB (HIV derived from a pool
of American patients) at a multiplicity of infection of 10,000 particles
per cell. Cells were incubated with 50 ttM [3H]azidothymidine
(424,000 dpm/nmol) for 24 hr either during infection or at the
indicated days during the infection cycle. Concentrations (AsM,
values in parentheses) of the phosphorylated derivatives of
azidothymidine were calculated using a packed cell volume of 8.5 x
108 cells/ml of packed cells.
Phosphorylation of Azidothymidine with Cell Extracts and
Purified Enzymes. The structural similarity ofazidothymidine
to thymidine suggested that thymidine kinase might catalyze
the phosphorylation of the analog. With cell extracts, thymidine and azidothymidine (100 jIM) were phosphorylated at
rates of 26 and 17 pmol/min per 106 viable cells, respectively.
There was no apparent virus stimulation of these activities
during a 10-day course of HIV infection.
Phosphorylation of azidothymidine catalyzed by cell extracts was totally inhibited by thymidine; conversely, thymidine phosphorylation was inhibited by azidothymidine.
This suggested that azidothymidine and thymidine phosphorylations were catalyzed by a single enzyme. Calculations
from inhibition data, assuming an apparent Km value for
thymidine of 3 AM (20), indicated a K1 value for azidothymidine of about 2 ,uM. The values of relative phosphorylation
rates and apparent Km values from extracts were compared
to values obtained with purified cytosolic thymidine kinase
from H9 cells. Azidothymidine was phosphorylated with a
maximal velocity 60% that of thymidine and with an apparent
Km value of 3.0 ,M compared to a value of 2.9 ,uM for
thymidine (Table 4). These phosphorylation rates and Km
values determined with the purified enzyme corresponded
well with the values obtained with the cell extracts (see
above). This correspondence supported the premise that
azidothymidine phosphorylation is catalyzed by the thymidine kinase of the cell.
Phosphorylation of azidothymidine monophosphate to the
diphosphate appeared to be catalyzed by thymidylate kinase.
Phosphorylation of azidothymidylate catalyzed by extracts
Table 3. Levels of phosphate derivatives after azidothymidine
removal
Time after Concentration, pmol per 106 cells (,uM)
removal, hr Monophosphate Diphosphate Triphosphate
0 820 (990) 5.5 (6.6) 6.0 (7.2)
0.5 300 (360) 6.0 (7.2) 4.3 (5.2)
1 190 (230) 1.3 (1.6) 1.6 (1.9)
2 42 (50) 0.7 (0.8) 1.4 (1.7)
4 13 (16) 0.6 (0.7) 0.8 (1.0)
H9 cells were infected with HTLV-IIIB (104 particles per cell; see
legend to Table 2) for 18 hr. At day 5 after infection, cells were
incubated for 24 hr with 50 ,uM [3H]azidothymidine (541,680
dpm/pmol). The [3H]azidothymidine was then removed and the
cultures were washed three times with 12 ml of prewarmed medium.
Prewarmed medium lacking azidothymidine was added to the cultures. At the indicated times the cells were harvested and extracted,
and the extracts were assayed as described in Materials and
Methods.
Medical Sciences: Furman et aL8336 Medical Sciences: Furman et al.
Table 4. Kinetic values for the phosphorylation of
azidothymidine and azidothymidylate
Relative
Enzyme Substrate Km", /uM V=,, %
Thymidine kinase Thymidine 2.9 �� 0.5 100
Azidothymidine 3.0 �� 0.3 60
Thymidylate kinase Thymidylate 4.1 �� 0.9 100
Azidothymidylate 8.6 �� 0.6 0.3
Purified thymidine kinase from the cytosol of H9 cells catalyzed
the conversion of radiolabeled thymidine or azidothymidine to the
5'-monophosphate. Product was separated from substrate by thinlayer chromatography with solvent I. Apparent Km (Km') values were
determined as described in Materials and Methods, with a 0.7-13 AM
range of substrate concentration. The values for thymidylate kinase
were determined as described in the legend to Fig. 2 and in the text.
of H9 cells was fully inhibited by the addition of dTMP (500
kLM). Further evidence that azidothymidylate bound to
dTMP kinase was obtained by measuring the effects of
azidothymidylate on the phosphorylation of dTMP with
purified dTMP kinase from cytosol extracts of H9 cells. The
results indicated that azidothymidylate is a competitive
inhibitor of dTMP phosphorylation, with a Ki, value of 8.6
,uM (Fig. 2). The apparent Km value for dTMP (4.1 ,uM) was
identical to that determined by Chen et al. (21) with enzyme
from Vero cells.
Radiolabeled azidothymidylate was used to compare the
rate of phosphorylation to that of dTMP. Its relative maximal
velocity was very low (Table 4). In spite of the low rate, since
azidothymidylate was a substrate for the dTMP kinase, its
apparent Km value is equal to the Kis value determined above
(22). Because of the low Km and low Vmax values for
azidothymidylate, it can be considered an effective alternative-substrate inhibitor of dTMP kinase.
Effect of Azidothymidine on Intracellular Deoxyribonucleoside Triphosphate Levels. In three separate experiments,
intracellular dTTP levels were severely reduced when H9
cells were exposed to 50 ,uM azidothymidine (Table 5). In
addition to the effect on dTTP levels, a large decrease in
0.50 0.75
1/[dTMP] (,uM)
FIG. 2. Inhibition of thymidylate kinase by azidothymidylate
(azdTMP). Purified cytosolic thymidylate kinase from H9 cells
(150 units/ml) of reaction mixture) catalyzed the conversion of
[14C]dTMP to [14C]dTDP. Reaction rates are expressed as pmol of
dTDP formed per min. Thin-layer chromatography with solvent II
was used to separate product from substrate. Other details were as
described in Materials and Methods. The Ki, value for azidothymidylate was determined from the replot of the slopes vs. azidothymidylate (azdTMP) concentration (Inset).
dCTP levels, an -50% decrease in dGTP levels, and a
significant increase in dATP levels were observed. A similar
trend was observed with HIV-infected H9 cells after exposure to 50 ,M azidothymidine for 24 and 48 hr during the
period of peak virus replication (days 5-7).
Azidothymidine Triphosphate Inhibits [3H]dTMP Incorporation Catalyzed by HIV Reverse Transcriptase. The ability of
azidothymidine triphosphate to bind to the HIV reverse
transcriptase and DNA polymerase a of H9 cells was assessed by measuring the inhibition of [3H]dTMP incorporation into a primer-template by azidothymidine triphosphate. The reverse transcriptase was much more sensitive to
inhibition than was the DNA polymerase a of H9 cells (Fig.
3). The IC50 values for the viral reverse transcriptase were 0.7
,uM with poly(rA).oligo(dT)12.18 and 2.3 ,uM with activated
calf thymus DNA as primer-templates. In contrast, an IC50
value of 260 ,uM was determined for azidothymidine triphosphate with the H9 DNA polymerase a when activated calf
thymus DNA was used as primer-template. The kinetics of
inhibition of the HIV reverse transcriptase by azidothymidine triphosphate were also determined using standard


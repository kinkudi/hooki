import webapp2
from wordgravity import jmain
import os
import json


class List(webapp2.RequestHandler):
	def get(self):
		path = './tests/ner'
		listing = os.listdir(path)
		summary = []
		self.response.out.write('<html><head><title>Hooki Results</title></head><body>') 
		for ifile in listing:
			if ifile[-1] == '~':
				continue
			ifilepath = '/'.join([path,ifile])
			data = json.load(open(ifilepath,'r'))
			for instance in data:
				summary.append(''.join(['<h1>',ifilepath,'</h1>',jmain(instance['text'])]))
			self.response.out.write(''.join(['<br>'.join(summary),'</body></html>']))
			summary = []

class Eval(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		path = './tests/ner'
		listing = os.listdir(path)
		summary = []
		output = ['<html><head><title>Hooki Results</title></head><body>'] 
		wholeavg = 0.0
		wholeacc = 0.0
		processed = []
		for ifile in listing:
			if ifile[-1] == '~':
				continue
			ifilepath = '/'.join([path,ifile])
			processed.append(ifilepath)
			data = json.load(open(ifilepath,'r'))
			accavg = 0.0
			fileaverage = 0.0
			lendata = 0
			for instance in data:
				text = instance['text']
				nes = instance['nes']
				if len(text) < 10:
					continue
				if len(nes) < 10:
					continue
				lendata += 1
				joutput,score,acc,m3 = jmain(text,75,True,nes)
				accavg += acc
				text = m3.process()
				fileaverage += score
				summary.append([''.join(['<h3>',ifilepath,'</h3>','<p>',text,'</p>',joutput]),score,acc])
			fileaverage /= (0.5 + lendata)
			accavg /= (0.5 +lendata)
			wholeavg += fileaverage
			wholeacc += accavg
			output.append(''.join([
				'<br>',
				'<a name=',ifilepath,'><h1>',ifilepath,'</h1></a>',
				'<h3><a href=#menu>Back to top!</a></h3>',
				'<h3>File average: ',`fileaverage`,' %</h3>',
				'<h3>File accuracy: ',`accavg`,' %</h3>',
				'<br>',
				'<br>'.join([
					''.join([
						s[0],
						'<br><h3>Text NE performance : ',`s[1]`,'%</h3>'
						]) 
					for s in sorted(summary,key=lambda(x):x[1],reverse=True)
						])
					]))

			summary = []
		wholeavg /= (len(processed))
		wholeacc /= (len(processed))
		output.append('</body></html>')
		output.insert(1,''.join([
			'<ul>',
			'</li>'.join([''.join([
				'<li><a href=#',p,'><h3>',p,'</h3></a>'
				])
				for p in processed
				]),
			'</li></ul>',
			'<br><br>'
			])
			)
		output.insert(1,''.join([
			'<h1><a name=menu>All files</a></h1>',
			'<h1>Recognition over all tests : ',`wholeavg`,' % </h1>',
			'<h1>Accuracy over all tests : ',`wholeacc`,' % </h1>'
				])
				)
		self.response.out.write(''.join(output))
		return 

app = webapp2.WSGIApplication([
	(r'/list.*',List),
	(r'/ner.*',Eval)
	])

def main():
	app.run()

if __name__ == "__main__":
	main()



